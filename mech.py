from random import randint, choice


class Mechanic(object):
    field = []

    def __init__(self, height, width):
        self.height = height
        self.width = width
        self.create_field()
        self.start()

    def start(self):
        self.point()
        self.point()

    def create_field(self):
        for x in range(self.width):
            self.field.append([0] * self.height)

    def point(self):
        x = randint(0, self.height - 1)
        y = randint(0, self.width - 1)
        if self.field[x][y] == 0:
            self.field[x][y] = choice([2, 4])
        else:
            self.point()

    def print_field(self):
        for row in self.field:
            print "".join(str(row))

